## Customization directory for service: Jenkins

This directory is meant to hold extensions for a service that helps customize it for specific cases, rather than overall.

An example would be for Jenkins to define a `plugins.txt`, some init scripts, or Job DSL scripts.

Questionable if this should exist as opposed to those functions being pointed to by service/stack config.

This would allow pointing to an external repo per tenant to customize, or even inlining plugins via yaml.