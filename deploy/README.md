## Deploy directory for service: Jenkins

This directory holds base deployment related files for this service, additionally including a Kustomize file for deploying the service standalone for testing.

A service may supply some k8s resource files that can be consumed immediately by a user with `kubectl` but then they're providing the context.

A stack should provide context by supplying a deployment type usable by this framework along with the logic by which to deploy to a given environment.
