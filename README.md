## Sample Jenkins Service

This directory wraps Jenkins as a service with a variety of potential extensions.

### Manual deploy test

Steps to manually run the example via Kustomize (with `mk` as an alias for `microk8s kubectl`):

* `cd /vagrant/services/jenkins/deploy` - to get into the right directory
* `mk create namespace jenkins` - to get a namespace
* `mk -n jenkins apply -k .` - to apply the Kubernetes resource files via Kustomize to the cluster
* Now reach Jenkins on the host OS at http://localhost:31005 (being forwarded to guest port 8080 by Kubernetes)
* See pods with `mk get pods -A`
* Find Jenkins initial password with `mk -n jenkins logs [jenkins pod name]`

This approach uses a default volume setup that's ephemeral and won't save changes after pod goes poof

Inspired by and adjusted from https://phoenixnap.com/kb/how-to-install-jenkins-kubernetes

### Automatic deploy test via Kustomize

Everything for this is contained within the `g2` scripting. In this case Kustomize adds in a persistent volume and may change a port.

* `cd /vagrant` - need to run `g2` commands from here.
* `./g2 service deploy jenkins jenkinsLocal1` - note: will clash if above manual process ends up occupying port 31005.
* `./g2 service deploy jenkins jenkinsLocal2` - this one should be able to get along even if the manual process is followed. Uses port 31006
* See pods with `mk get pods -A` and get the password if desired from the right pod and namespace.

This will set up two subtly different instances of Jenkins in different namespaces
