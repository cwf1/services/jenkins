## App directory for service: Jenkins

This is an optional directory that'll hold a service's app files in the case of them needing to be customized.

In some cases you won't need this, if an original Docker image for instance can be customized without changes.

Alternatively to this directory you can define the target app via `service.yaml` in the root of the service dir.